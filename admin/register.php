﻿
<?php
if ($_POST['ok']){
    require_once ("../db.php");

    $name = htmlspecialchars($_POST['login']);
    $password1 = htmlspecialchars($_POST['pasw1']);
    $password2 = htmlspecialchars($_POST['pasw2']);

    if (R::count('registered', "login = ?", array($name)) > 0){
        echo "<h3 class='er'><b>Такий логін вже використовується,\n оберіть інший.</b></h3>";

    }else if ($password1 === $password2){

        $date = date( "d.m.Y H:i" );
        $link = R::dispense( 'registered' );
        $link->login = $name;
        $link->password = password_hash($password1, PASSWORD_DEFAULT);

        $result = R::store($link);

        $_SESSION['user'] = $_POST['login']; // за допомогою сесії для зареєстрованих користувачів є додаткові можливості
        header('Location: ../index.php');
       // echo "<h3><a href='../index.php'>Вхід дозволено проходьте</a></h3>";
    }
    else echo "<h2 class='er'>Пароль не співпадає</h2>" ;
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="Content-Type: text/html; charset=utf-8">
    <title>Вхід</title>
    <link rel="stylesheet" href="../style.css">
</head>
<body>

<h3 style="margin-left: 30%"> Для отримання додаткових можливостей зареєструйтесь </h3>
<br/>
<br/>
<h2 style='color: darkgreen'><a href='../index.php'>На головну сторінку</a></h2>
<div style="background-color: #D3D3D3; width:200px; padding:10px; margin-left: 550px">
    <form name="register" action="" method="post">
        <lebel> Логін або емейл </lebel> <br>
        <input type="text" name="login" placeholder="Емейл плиз" required value=<? echo $_POST['login']?>>
        <br>
        <br>
        <lebel> Пароль  </lebel> <br>
        <input type="password" name="pasw1" title="пароль" required>
        <br>
        <br>
        <lebel> Введіть тойже пароль ще раз </lebel> <br>
        <input type="password" name="pasw2" title="пароль" required>
        <br>
        <br>
        <input type="submit" class="a" name="ok" value="Відправити"> <br>

    </form>
</div>
</body>
</html>
