<?php

    require_once("../db.php");
    require_once("../models/articles.php");
        
    if (isset($_GET['action']))
        $action = $_GET['action'];
    else
        $action = "";

    if ($action == "add") {
        if(!empty($_POST)){
            articles_new($_POST['title'], $_POST['content'], $_SESSION['image']);
            header("Location: adminka.php");
        }
        include("../views/article_admin.php");

    } else if ($action == "edit") {

       if(!isset($_GET['id'])){
            header('Location:  adminka.php');}

        $id = (int)$_GET['id'];  ///

        if ($_POST['ok']&& $id > 0)  {
            articles_edit($id, $_POST['title'], $_POST['content'], $_SESSION['image']);
            header("Location: adminka.php");
        } else {
            $article = articles_get($_GET['id']);
             include("../views/article_admin.php");
        }

    } else if ($action == "delete"){
        $id = $_GET['id'];
        $article = articles_delete($id);
        header('Location: adminka.php');
    }

    else {
        $articles = articles_all();
        include("../views/articles_admin.php");
    }


?>