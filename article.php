<?php
$title = "Блог Романюка";

require "db.php";
require_once("models/articles.php");

$article = articles_get($_GET['id']);

require "views/header.php";
require "views/article.php";

if (isset($_SESSION['user'])){
    require "views/comments.php";
}

require "views/footer.php";