﻿<?php

function articles_delete($id){

    $id = (int)$id;

    $result = R::exec("DELETE FROM articles WHERE id='$id'");

    return $result;
}

function articles_edit($id, $title, $content, $image){

    $date = date( "d.m.Y H:i" );
    $result = R::exec("UPDATE articles SET title = '$title' , content = '$content' , date = '$date', image = '$image' WHERE id = '$id'");

    if (!$result) die(mysqli_error($result));
    return $result;
}

function articles_ed($id, $image){

    $date = date( "d.m.Y H:i" );
    $result = R::exec("UPDATE articles SET image = '$image' WHERE id = '$id'");

    if (!$result) die(mysqli_error($result));
    return $result;
}


function articles_all(){

    $articles = R::getAll("SELECT * FROM articles");

    return $articles;
}

function all_comments($page_id){

    $all_comments = R::getAll("SELECT * FROM coments WHERE page_id = '$page_id'");
    return $all_comments;
}

function articles_get($id)
{
    $result = R::LOAD('articles', $id);
    return $result;
}

function articles_new($title, $content, $image){
    if ($title == '')
        return false;
    $date = date( "d.m.Y H:i" );
    $link = R::dispense( 'articles' );
    $link->title = $title;
    $link->date = $date;
    $link->content = $content;
    $link->image = $image;

    $result = R::store($link);
    if (!$result)
        die(mysqli_error($link));

    return true;
}

function articles_intro($text, $len = 400)
{
    return mb_substr($text, 0, $len);
}

function update_comments($page_id, $name, $text_comment)
{
    $date = date("d.m.Y H:i");
    $blog = R::dispense('coments');
    $blog->page_id = $page_id;
    $blog->date = $date;
    $blog->name = $name;
    $blog->text_comment = $text_comment;

    $id = R::store($blog);
    if (!$id) die(mysqli_error($id));
}


