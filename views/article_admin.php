<?php
    if ($_POST['pres']){

        if($_FILES["filename"]["size"] > 1024*3*1024)
        {
            echo ("Размер файла превышает три мегабайта");
            exit;
        }

        // Проверяем загружен ли файл
       /* */

       if (is_uploaded_file($_FILES["filename"]["tmp_name"]))
        {
            // Если файл загружен успешно, перемещаем его из временной директории в конечную
            move_uploaded_file($_FILES["filename"]["tmp_name"], "../library/image/".$_FILES["filename"]["name"]);
            $_SESSION['image'] =  "../library/image/".$_FILES["filename"]["name"];
            //$foto = "../library/image/".$_FILES["filename"]["name"];
            articles_ed($article['id'] , $_SESSION['image']);
           // articles_all();
            header("Location: ".$_SERVER["HTTP_REFERER"]);// Делаем реридект

        } else {
            echo("Ошибка загрузки файла");
        }
    }
?>

<!DOCTYPE html>
<hmtl>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="../style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="../library/ckeditor/ckeditor.js"></script>
    </head>
    <body>
        <div class="container">
            <h4><a href="../index.php"> Блог Олександра Романюка</a></h4>
            <div>
                <br>
                <label style="font-family: 'Arial Black'; font-size: medium">Додати мініатюру</label><br>

                <tbody style="height: 100%; width:100%;">

                <form action="" method="post" enctype="multipart/form-data">

                    <tr>
                        <td style="width: 100px;"><img style="float: left; margin-outside: auto"
                            src=" <?=$article['image']?>"
                        alt="" width="124" height="89" /><span id="transmark" style="display: none; width: 0px; height: 0px;"></span>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="file" name="filename"> Назва файлу повинна містити тільки латинські літери <br><br>
                            <input type="submit" name="pres" value="Загрузить">
                        </td>
                    </tr>
                </form>
                </tbody>
                <br><br>
                <form method="post" action="adminka.php?action=<?=$_GET['action']?>&id=<?=$_GET['id']?>">
                    <label style="font-family: 'Arial Black'; font-size: medium">Назва статті</label><br>
                        <input type="text" name="title" value="<?=$article['title']?>" class="form-item" autofocus required>

                    <br><br>

                    <label style="font-family: 'Arial Black'; font-size: medium">Текст статті</label><br>
                        <textarea class="form-item" name="content" required><?=$article['content']?></textarea>
                    <script> CKEDITOR.replace("content") </script>
                    <br>
                    <input type="submit" name="ok" value="Сохранить" class="btn">
                </form>
            </div>
        </div>

    </body>
</hmtl>