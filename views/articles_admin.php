<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8">
      <title>Блог</title>
      <link rel="stylesheet" href="../style.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
        <h4><a href="../index.php">Блог Олександра Романюка</a></h4>
        <h2 style="font-family: 'Comic Sans MS'; margin-left: 20%">Розділ для роботи зі статтями</h2><br>
        <div>
          <b><p><h3><a href="adminka.php?action=add"> Добавити статтю</a></h3></p></b><br>
          <table class="admin-table">
            <tr>
                <th>Дата</th>
                <th>Заголовок</th>
                <th></th>
                <th></th>
            </tr>
            <?php foreach($articles as $a): ?> 
              <tr>
                  <td><?=$a['date']?></td>
                  <td><?=$a['title']?></td>
                  <td>
                      <a href="adminka.php?action=edit&id=<?=$a['id']?>">Редагувати</a>
                  </td>
                  <td>
                      <a href="adminka.php?action=delete&id=<?=$a['id']?>">Видалити</a>
                  </td>
              </tr>
            <?php endforeach ?>
          </table>        
        </div>
     </div>
  </body>
</html>