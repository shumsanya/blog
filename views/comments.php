<?php

    if ($_POST['ok']){
       // print_r($_POST);

        $name = ($_POST['name']);
        $page_id = $_POST["page_id"];
        $text_comment = htmlspecialchars($_POST['text_comment']);
        $date = date( "d.m.Y H:i" );

        update_comments($page_id, $name, $text_comment);

        header("Location: ".$_SERVER["HTTP_REFERER"]);// Делаем реридект

    }

?>

<div class="comment">
    <?php $all_comments = all_comments($article['id']); ?>
    <form name="comment" action="" method="post">

            <input type="hidden" name="name" value="<?=$_SESSION['user']?>" />
        <p>
            <label>Ваш коментар <?=$_SESSION['user']?> :</label>
            <br />
            <textarea class="cont" name="text_comment" cols="100" rows="3"></textarea>
            <input type="submit" name="ok" value="Отправить" />
        </p>
        <p>
            <input type="hidden" name="page_id" value="<?=$article['id']?>" />

        </p>
    </form>
    <div>
        <?php foreach ($all_comments as $a): ?>
            <div class="c">

                <b><em style="color: gray">автор - <?=$a['name']?>  опубліковано: <?=$a['date']?></em></b>
                <p><?=$a['text_comment']?></p>
            </div>
        <?php endforeach ?>
    </div>
</div>